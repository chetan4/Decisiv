class DeliveryMailer < ActionMailer::Base
  default from: "admin@decisiv.net",
          to: User.where('publisher =?',true).select(:email).collect(&:email)
              
  def upload_finished(file)
    @file = file
    @type = file.type_of == "PartPrice" ? "Part Price" : "Market Basket"
    mail(:subject => "File Uploaded and Parsing done")
  end
  
  def published_completed(filenames,typeof)
    @filenames = filenames
    @type = typeof == "PartPrice" ? "Part Price" : "Market Basket"
    mail(:subject => "Files Publishing done ")
  end
  
  def published_failure(filenames,error,typeof)
    @filenames = filenames
    @error = error
    @type = typeof == "PartPrice" ? "Part Price" : "Market Basket" 
    mail(:subject => "File Publishing Failed ")
  end
  
  def upload_failure(file) 
    @file = file
    @type = file.type_of == "PartPrice" ? "Part Price" : "Market Basket"
    mail(:subject => "File Uploaded Complete but Parsing Failed")    
  end
  
  def file_uploaded(file)
    @file = file
    @type = file.type_of == "PartPrice" ? "Part Price Exchange File" : "Market Basket Exchange File"
    mail(:subject => "#{file.name} Uploaded Successfully")
  end
  
   def user_creation_email(user_created,password)
    @user     = user_created
    @password = password
    mail(:to => user_created.email,:subject => "Decisiv Registration") 
  end
end


