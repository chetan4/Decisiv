class AuditsController < ApplicationController
  authorize_resource
  
  before_filter :filter_params
  def index
    if @where_clause.empty?
      @audits = Audit.paginate(:page => params[:page])
    else
      @audits = Audit.where(@where_clause.join(" AND "),*@where_value).paginate(:page => params[:page])
    end 
  end  
  
  
  def export_to_xls

    if @where_clause.empty?
      @audits = Audit.select(['audits.operation','audits.performed_on','audits.description','audits.logged_time'])
    else
      @audits = Audit.where(@where_clause.join(" AND "),*@where_value).select(['audits.operation','audits.performed_on','audits.description','audits.logged_time'])
    end 
    send_data(@audits.to_xls,:filename => 'audits.xls')
   
  end
  
  private
  def filter_params
    @where_clause,@where_value = [[],[]]
    if params[:audits].present?
      params[:audits].each do |key,value|
        unless value.empty?
          if key == "start_date" || key == "end_date"
            if key == "start_date"
              @where_clause << "logged_time >= ?"
              @where_value <<  value # DateTime.new(*(params[:audits][:start_date].split("/").collect(&:to_i))).to_s
            elsif key == "end_date"
              @where_clause << "logged_time <= ?" 
              @where_value << value #DateTime.new(*(params[:audits][:end_date].split("/").collect(&:to_i))).to_s
            end
          else
            @where_clause << "#{key}=?"
            @where_value << value
          end 
        end
      end
    end
  end
end
