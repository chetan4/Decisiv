class UsersController < ApplicationController
  authorize_resource

 def index
    @users = User.scoped
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(params[:user])
    password = params[:user][:password]
    respond_to do |format|
      if @user.save
        @user.deliver_creation_email(password)
        flash[:message] = "User Successfull Created"
        format.html { redirect_to thank_you_path }
      else
        format.html { render action: "new" }
      end
    end  
  end
  
  def edit
    @user = User.find(params[:id])
    authorize! :edit, @user 
  end
  
  def update
    @user = User.find(params[:id])
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end
    params[:user].delete(:publisher) unless current_user.publisher? 
    authorize! :update, @user 
    respond_to do |format|
      if @user.update_attributes(params[:user])
        flash[:message] = "User Updated Successfully"
        if (current_user.id == @user.id)
           sign_in(@user, :bypass => true)
        end   
        format.html { redirect_to thank_you_path }
      else
        format.html { render action: "edit" }
      end
    end
  end
  

  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_path }
    end
  end
  
end
