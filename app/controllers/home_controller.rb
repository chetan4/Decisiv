class HomeController < ApplicationController
  skip_authorization_check :only => [:index]
  authorize_resource :class => false ,:only => [:thank_you]
  def index
  end
  
  def thank_you
  end
end
