class BasicFilesController < ApplicationController
  # GET /basic_files
  # GET /basic_files.json
  include AuditLogger
  authorize_resource
  def index
 
   @basic_files = BasicFile.where('import_status=? AND status =?','uploaded','na')  
    ## @basic_files = BasicFile.group(:type_of)
    respond_to do |format|
      if request.xhr?
        format.html {render :partial => 'upload_done',:layout => false}
      else  
        format.html 
      end
    end
  end

  def uploads
    
  end
  
  def upload_failed
    @basic_files = BasicFile.where('import_status=? AND status =?','failed','na')
    respond_to do |format|
      format.html { render :layout => false }
    end
  end
  
  def upload_in_progress
     @basic_files = BasicFile.where('import_status=? and status= ?','uploading','na')
     respond_to do |format|
        format.html { render :layout => false }
     end
  end
  # GET /basic_files/1
  # GET /basic_files/1.json
  def show
    @basic_file = BasicFile.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @basic_file }
    end
  end

  # GET /basic_files/new
  # GET /basic_files/new.json
  def new_part_price_file
    @basic_file = BasicFile.new
    respond_to do |format|
      format.html
      format.js { render :layout => false }
    end
  end


  def new_market_basket_file
    @basic_file = BasicFile.new
    respond_to do |format|
      format.html 
      format.js { render :layout => false }
    end
  end
  
  
  # GET /basic_files/1/edit
  def edit
    @basic_file = BasicFile.find(params[:id])
  end

  # POST /basic_files
  # POST /basic_files.json
  def create 
   # name = params[:basic_file][:name]
    
    @basic_file = BasicFile.find_by_name_and_type_of(params[:basic_file][:name],params[:basic_file][:type_of]) || BasicFile.new(params[:basic_file])
    operation = @basic_file.new_record? ? "create" : "update"
    respond_to do |format|
        if (@basic_file.new_record? ?  @basic_file.save : @basic_file.update_attributes(params[:basic_file].merge('import_status' => 'na',:status => 'na',:published_at => nil)))
          #DeliveryMailer.file_uploaded(@basic_file).deliver
          @basic_file.start_background_processing(current_user.id) 
          audit_log(operation,@basic_file.type_of,"#{current_user.name} created a #{@basic_file.type_of}File")
          format.html { redirect_to @basic_file } 
        else
          format.html { render :action => @basic_file.template_to_render  }
        end
      end
    
    end

  
  
  def validation_error
    @basic_file = BasicFile.find(params[:id])
    @validation_errors = JSON.parse($redis.get("basic_file_#{@basic_file.id}")) rescue [] #@basic_file.validation_errors
    respond_to do |format|
      format.html { render :layout => false }
    end
  end 
  # PUT /basic_files/1
  # PUT /basic_files/1.json
  def update
    @basic_file = BasicFile.find(params[:id])
    respond_to do |format|
      if @basic_file.update_attributes(params[:basic_file].merge('import_status' => 'na',:status => 'na',:published_at => nil))
        #DeliveryMailer.file_uploaded(@basic_file).deliver
        #@basic_file.clear_any_validation_errors
        @basic_file.start_background_processing(current_user.id) 
        audit_log("update",@basic_file.type_of,"#{current_user.name} created a #{@basic_file.type_of}File")
        format.html { redirect_to @basic_file } 
      else
      
        format.html { render :action => 'edit'}
      end  
    end
  end

  # DELETE /basic_files/1
  # DELETE /basic_files/1.json
  def destroy
    @basic_file = BasicFile.find(params[:id])
    #audit_log("delete",@basic_file.type_of,"#{current_user.name} deleted a #{@basic_file.type_of}File") 
    @basic_file.destroy

    respond_to do |format|
      format.html { redirect_to basic_files_url }
      format.json { head :no_content }
    end
  end
  
  
  def publish_completed
    @basic_files = BasicFile.where('status=?','published')
    respond_to do |format|
      if request.xhr?
        format.html { render :partial => "publish",:layout => false }
      else
        format.html
      end
    end
  end
  
  def publish_failed
    @basic_files = BasicFile.where('status=?','failed')
    respond_to do |format|
      format.html { render :partial=> 'publish',:layout => false }
    end  
  end
  
  def publish_in_progress
    @basic_files = BasicFile.where('status =?','publishing')
    respond_to do |format|
      format.html { render :partial=> 'publish',:layout => false }
    end
  end
  
  def publish

    files = BasicFile.where('id in (?)',params[:publish])
   
    respond_to do |format|
        BasicFile.publish_file(files) 
        audit_log("publish",files)
        flash[:message] = "Files are marked for Publishing ..."
        format.html { redirect_to thank_you_path }
     end   
 
  end
  
  def republish
    file = BasicFile.find(params[:id])
  
    respond_to do |format|
      if file.failed?
        audit_log("republish",file.type_of,"#{current_user.name} Republished a #{file.type_of}File") 
        BasicFile.publish_file([file])
        flash[:field_name] = "success"
        flash[:notice] = "#{file.name} is marked for Publishing"  
      else
        flash[:field_name] = "error"
        flash[:error] = "Cannot Republish a file which is not failed"  
      end 
      format.html { redirect_to publish_completed_basic_files_path }
    end
  end
  
  def download_page
    
  end
  
  
  def downloadable_part_price_file
   
    @downloaders = SubscriberDownloader.where('file=? and status=?',"PartPrice","completed").order("id desc")
  end
  
  def downloadable_market_basket_file
    @downloaders = SubscriberDownloader.where('file=? and status=?',"MarketBasket","completed").order("id desc")
  end
  
  #def download_part_price_file
    #audit_log("download","PartPrice","#{current_user.name} downloaded a PartPriceFile")
    #current_user.update_column('part_price_downloaded_at',DateTime.now)
    #redirect_to s3_url('part_price_file.csv')
  #end
  
  #def download_market_basket_file
    #audit_log("download","MarketBasket","#{current_user.name} downloaded a MarketBasketFile")
    #current_user.update_column('market_basket_downloaded_at',DateTime.now)
    #redirect_to s3_url('market_basket_file.csv')
  #end
  
  def download
    file =  SubscriberDownloader.find(params[:id])
    audit_log("download",file.name,"#{current_user.name} downloaded #{file.file}File")
    #$storage.directories.get($BUCKET).files.get(File.join(basicfile.asset.store_dir,basicfile.asset.filename)).body
    send_data(s3_data_for_file(file.name),:filename => file.name)
    #redirect_to s3_url(file.name) 
  end
  
  private 
  def s3_data_for_file(filename)
    $storage.directories.get($BUCKET).files.get("uploads/subscriber/#{filename}").body
  end
  
  def s3_url(filename)
    expires_at = Time.now + (5 * 60)
    $storage.directories.get($BUCKET).files.get_url("uploads/subscriber/#{filename}", expires_at)
  end
end
