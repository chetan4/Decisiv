class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authenticate_user!
  before_filter :user_time_zone
  check_authorization :unless => :devise_controller?

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to home_url, :alert => exception.message
  end
  
 private
  def after_sign_in_path_for(resource)
    home_path
  end

  def after_sign_out_path_for(resource)
    login_path
  end
  
  def user_time_zone
    if request.cookies["utc_offset"].present?
     session[:time_zone_offset] ||= -(request.cookies["utc_offset"].to_i * 60)
     Time.zone = ActiveSupport::TimeZone[session[:time_zone_offset]]
    end 
  end
end


