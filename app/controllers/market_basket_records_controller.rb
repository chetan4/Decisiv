class MarketBasketRecordsController < ApplicationController
  # GET /market_basket_records
  # GET /market_basket_records.json
  def index
    @market_basket_records = MarketBasketRecord.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @market_basket_records }
    end
  end

  # GET /market_basket_records/1
  # GET /market_basket_records/1.json
  def show
    @market_basket_record = MarketBasketRecord.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @market_basket_record }
    end
  end

  # GET /market_basket_records/new
  # GET /market_basket_records/new.json
  def new
    @market_basket_record = MarketBasketRecord.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @market_basket_record }
    end
  end

  # GET /market_basket_records/1/edit
  def edit
    @market_basket_record = MarketBasketRecord.find(params[:id])
  end

  # POST /market_basket_records
  # POST /market_basket_records.json
  def create
    @market_basket_record = MarketBasketRecord.new(params[:market_basket_record])

    respond_to do |format|
      if @market_basket_record.save
        format.html { redirect_to @market_basket_record, notice: 'Market basket record was successfully created.' }
        format.json { render json: @market_basket_record, status: :created, location: @market_basket_record }
      else
        format.html { render action: "new" }
        format.json { render json: @market_basket_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /market_basket_records/1
  # PUT /market_basket_records/1.json
  def update
    @market_basket_record = MarketBasketRecord.find(params[:id])

    respond_to do |format|
      if @market_basket_record.update_attributes(params[:market_basket_record])
        format.html { redirect_to @market_basket_record, notice: 'Market basket record was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @market_basket_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /market_basket_records/1
  # DELETE /market_basket_records/1.json
  def destroy
    @market_basket_record = MarketBasketRecord.find(params[:id])
    @market_basket_record.destroy

    respond_to do |format|
      format.html { redirect_to market_basket_records_url }
      format.json { head :no_content }
    end
  end
end
