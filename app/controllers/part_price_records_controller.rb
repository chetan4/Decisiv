class PartPriceRecordsController < ApplicationController
  # GET /part_price_records
  # GET /part_price_records.json
  def index
    @part_price_records = PartPriceRecord.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @part_price_records }
    end
  end

  # GET /part_price_records/1
  # GET /part_price_records/1.json
  def show
    @part_price_record = PartPriceRecord.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @part_price_record }
    end
  end

  # GET /part_price_records/new
  # GET /part_price_records/new.json
  def new
    @part_price_record = PartPriceRecord.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @part_price_record }
    end
  end

  # GET /part_price_records/1/edit
  def edit
    @part_price_record = PartPriceRecord.find(params[:id])
  end

  # POST /part_price_records
  # POST /part_price_records.json
  def create
    @part_price_record = PartPriceRecord.new(params[:part_price_record])

    respond_to do |format|
      if @part_price_record.save
        format.html { redirect_to @part_price_record, notice: 'Part price record was successfully created.' }
        format.json { render json: @part_price_record, status: :created, location: @part_price_record }
      else
        format.html { render action: "new" }
        format.json { render json: @part_price_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /part_price_records/1
  # PUT /part_price_records/1.json
  def update
    @part_price_record = PartPriceRecord.find(params[:id])

    respond_to do |format|
      if @part_price_record.update_attributes(params[:part_price_record])
        format.html { redirect_to @part_price_record, notice: 'Part price record was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @part_price_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /part_price_records/1
  # DELETE /part_price_records/1.json
  def destroy
    @part_price_record = PartPriceRecord.find(params[:id])
    @part_price_record.destroy

    respond_to do |format|
      format.html { redirect_to part_price_records_url }
      format.json { head :no_content }
    end
  end
end
