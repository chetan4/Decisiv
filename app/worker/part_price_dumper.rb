class PartPriceDumper
  @queue = :part_price_dumper
  
   def self.perform(record_id,user_id)
    begin
      basicfile = BasicFile.find(record_id)
      basicfile.update_column('import_status','uploading')
      $redis.del("basic_file_#{basicfile.id}")
      #basicfile.clear_any_validation_errors
      download_file_from_s3(basicfile)
      part_price_attributes = Array.new
      part_numbers = Array.new
      part_number_index = 0
      log_records = []
      log_time = nil
      part_price_records = Array.new
      deleted_part_price_records = Array.new
      validation_errors = Array.new 
      updated_part_numbers = Array.new 
      times  = 0
      row_no = 0
      part_price_file = true
      ## CSV.foreach(file,{:headers => true,:header_converters => :symbol}) do |row|
      ##  part_price_attributes =  row.headers if part_price_attributes.empty?
      ##  if PartPriceRecord.new(Hash.new[row.entries]).valid?
      ##    part_price_records  << row.fields
      ##  end
      ## end
      WorkingLogger.logger.info "[INFO] #{Time.now} --> Starting Parsing of Part Price File Parsing [#{basicfile.name}] ..."
      CSV.foreach(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s,basicfile.asset.filename.to_s)) do |row|
        row_no += 1
        ## row.collect(&:strip).collect(&:empty?).reduce(:&) slow in processing
        row_records = row.collect { |element| element.strip.downcase unless element.nil? }
        if row.empty? || (row_records.count == 1 && row_records.first.empty?) || row_records.collect(&:empty?).reduce(:&)
          next
        end 
        ##(row.collect(&:strip).count == 1 && row.collect(&:strip).first.empty?)
        if part_price_attributes.empty?
          part_price_attributes = row.collect(&:strip).push(:basic_file_id)
          if part_price_attributes.include?('price_level') || part_price_attributes.include?('customer_id')
            part_price_file = false
            break
          end
          part_number_index = part_price_attributes.index('part_number')
          next
        end
        
        record_hash = Hash.new
      
        part_price_attributes.each_with_index do |attrr,index|
          if attrr == :basic_file_id
            record_hash.merge!({:basic_file_id => basicfile.id})
          else
            record_hash.merge!({attrr.to_sym => row[index]})
          end
        end  

        record =  PartPriceRecord.new(record_hash)
        if row.push(basicfile.id).length == part_price_attributes.length && record.valid? && !part_numbers.include?(row[part_number_index]) 
          part_number =  row[part_number_index]
          deleted_part_price_records << PartPriceRecord.where(part_number: part_number).first unless PartPriceRecord.where(part_number: part_number).empty?
          part_price_records << record_hash
          part_numbers     <<   part_number
        else
          WorkingLogger.logger.debug "[DEBUG] #{Time.now} --> A Record was found to be invalid in Part Price File  [#{basicfile.name}]"
          record.errors.add :base,"Improper Rows fields" if row.length != part_price_attributes.length 
          record.errors.add :part_number,"Part Number already present in CSV File['Duplicate']" if part_numbers.include?(row[part_number_index]) 
          #validation_errors << [row_no,record.errors.messages.to_yaml,basicfile.id]
          validation_errors << {:row_no => row_no,:error_encountered => record.errors.messages.to_yaml}
          #ValidationError.create(:row_id => row_no , :error_encountered => record.errors.messages.to_yaml ,:basic_file_id => basicfile.id)
          times += 1
        end 
        record_hash,record,row_records = [nil,nil,nil] ## to Avoid Memory bloat 
      end
      
      if times.zero?  && part_price_file  
        puts "*****************************"
        puts deleted_part_price_records.empty?
        puts PartPriceRecord.where(:basic_file_id => basicfile.id).empty? 
        puts "******************************"
        unless deleted_part_price_records.empty? && PartPriceRecord.where(:basic_file_id => basicfile.id).empty?
          accumulator = deleted_part_price_records.clone
          
          log_time = DateTime.now 
          unless deleted_part_price_records.empty?
            accumulator.collect(&:attributes).collect do |element|
              element.delete("_id")
              element.delete("basic_file_id")
              log_records << [user_id,"update","Part Price","Record",element.to_json,log_time]
              element = nil
            end
            updated_part_numbers = accumulator.collect(&:part_number)
          end  
          ## Destroy the records
          PartPriceRecord.where(:_id.in => deleted_part_price_records.collect(&:_id).collect(&:to_s)).delete_all
          ## DETECT the filename and destroy old records
          stale_records = PartPriceRecord.where(:basic_file_id => basicfile.id)
          accumulator = stale_records.clone
          unless accumulator.empty?
            accumulator.collect(&:attributes).collect do |element|
              element.delete("_id")
              element.delete("basic_file_id") 
              log_records << [user_id,"delete","Part Price","Record",element.to_json,log_time]
              element = nil
            end
          end  
          stale_records.delete_all
          accumulator,log_time = nil,nil
        end 

        ## CREATE SUSBSCRIBER DOWNLOADER FILE --> DONE
        #if $redis.get("part_price").nil?
          ## put the params with the modified_file containing current basic_file_id 
          #SubscriberDownloader.create(:modified_file => {"id" => [basicfile.id]}.to_json)
          #$redis.set("part_price",true)
        #else
          ## define the method  ==> done
         # subscriber_downloader = SubscriberDownloader.latest("part_price_file").first
         ### define the attribute ==> done
          #modified_file = JSON.parse(subscriber_downloader.modified_file)
          #modified_file["id"].push(basicfile.id) unless modified_file["id"].include?(basicfile.id) 
          #subscriber_downloader.update_column("modified_file",modified_file.to_json)
        #end  
       # PartPriceRecord.destroy_all("_id in (?)",deleted_part_price_records) unless deleted_part_price_records.empty?
        log_time  = DateTime.now
        part_price_records.each do|rr|
          unless updated_part_numbers.include?(rr['part_number'])
            log_records << [user_id,"create","Part Price","Record",rr.to_json,log_time]
          end  
          puts rr.to_yaml
          PartPriceRecord.create(rr)
          rr=nil
        end  
        logging(log_records)
        #PartPriceRecord.collection.insert(part_price_records)
        ## RECORD AUDIT LOG
        ##PartPriceRecord.import(part_price_attributes,part_price_records,:validate => false)
        
        basicfile.update_attributes(:import_status => "uploaded",:attempt=> basicfile.attempt+1,:error_field => '',:uploaded_at => DateTime.now)
        WorkingLogger.logger.info "[INFO] #{Time.now} --> A Part Price File [#{basicfile.name}] Parsing was successful "
        part_price_records,updated_part_numbers,log_records = nil,nil,nil
      elsif part_price_file
        basicfile.update_attributes(:import_status => 'failed',:attempt => basicfile.attempt+1,:error_field => "#{times} record in the given csv file where incorrect",:uploaded_at => DateTime.now)
        #ValidationError.import([:row_id,:error_encountered,:basic_file_id],validation_errors,:validate => false)
        $redis.set("basic_file_#{basicfile.id}",validation_errors.to_json)
        validation_errors = nil
       WorkingLogger.logger.debug "[DEBUG] #{Time.now} --> A Part Price File [#{basicfile.name}] Parsing was failed"
      else
        basicfile.update_attributes(:import_status => 'failed',:attempt => basicfile.attempt+1,:error_field => "The attributes supplied to the file does not belong look like part price file attributes",:uploaded_at => DateTime.now)
       WorkingLogger.logger.debug "[DEBUG] #{Time.now} --> A Part Price File [#{basicfile.name}] Parsing was failed"
      end
      remove_directory(basicfile)
      send_an_email(basicfile)
    rescue => e

      WorkingLogger.logger.error "[ERROR] #{Time.now} --> The Part Price File [#{basicfile.name}] could not be parse citing error ==> #{e}"
   
      basicfile.update_attributes(:import_status => 'failed',:error_field => e.to_s ,:uploaded_at => DateTime.now)
      
      remove_directory(basicfile)
      send_failure_email(basicfile)
    end  
  end
  
  def self.logging(records)
    WorkingLogger.logger.info "INFO #{Time.now} --> Logging the Record for current file "
    Audit.import([:user_id,:operation,:performed_on,:entity,:description,:logged_time],records,:validate => false) unless records.empty?
    WorkingLogger.logger.info "INFO #{Time.now} --> Logged the Record for current file "
  end
  
  def self.download_file_from_s3(basicfile)
     WorkingLogger.logger.info "[INFO]  #{Time.now} --> Downloading Part Price File from S3 [#{basicfile.name}] ..."
    create_directory(basicfile)
    File.open(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s,basicfile.asset.filename.to_s),'w+') do |file|
      file << $storage.directories.get($BUCKET).files.get(File.join(basicfile.asset.store_dir,basicfile.asset.filename)).body
    end
     WorkingLogger.logger.info "[INFO]  #{Time.now} ---> Downloaded Part Price File from S3 [#{basicfile.name}] !!!  "
  end
  
  def self.create_directory(basicfile)
    WorkingLogger.logger.info "[INFO]  #{Time.now} ---> Creating Directory for Part Price File locally [#{basicfile.name}] ..."
    unless File.directory?(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s))
      FileUtils.mkdir_p File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s)
    end
     WorkingLogger.logger.info "[INFO]  #{Time.now} --> Created Directory for Part Price File locally [#{basicfile.name}] !!!"    
  end
  
  def self.remove_directory(basicfile)
   WorkingLogger.logger.info "[INFO] #{Time.now} --> Removing Directory after processing Part Price File [#{basicfile.name}] ..."
   
    FileUtils.rm_r File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s) if File.exists?(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s))
    
    WorkingLogger.logger.info "[INFO] #{Time.now} --> Removed Directory after processing Part Price File [#{basicfile.name}] !!!"
  end
  
  #def self.remove_the_local_file(basicfile)
  #  Rails.logger.info "Remove the basicfile"
  #  FileUtils.rm File.join(PUBLISHER_DIRECTORTY,basicfile.typeof.to_s,basicfile.asset.filename.to_s) if File.exists?(File.join(PUBLISHER_DIRECTORTY,basicfile.typeof.to_s,basicfile.asset.filename.to_s)) 
  #  Rails.logger.info "Removed the basicfiles"
  #end
  
  def self.send_an_email(basicfile)
   WorkingLogger.logger.info "[INFO] #{Time.now} Send email for Part Price File [#{basicfile.name}] ...  "
    #DeliveryMailer.upload_finished(basicfile).deliver
    WorkingLogger.logger.info "[INFO] #{Time.now} Email Send for Part Price File [#{basicfile.name}] !!!"
  end
  
  def self.send_failure_email(basicfile)
    WorkingLogger.logger.info "[INFO] #{Time.now} Delivering failure mail of Parsing of Part Price File [#{basicfile.name}] ..."
    #DeliveryMailer.upload_failure(basicfile).deliver
    WorkingLogger.logger.info "[INFO] #{Time.now} Delivered failure mail of Parsing of Part Price File [#{basicfile.name}] !!!"
  end
end
