class UserCreation
  @queue = :user_creation
  
  def self.perform(user_id,password)
    user_created = User.find(user_id)
    DeliveryMailer.user_creation_email(user_created,password).deliver
  end
end
