class PublishRecordJob
  @queue = :publish_record_job
  
  def self.perform(record_ids,typeof,downloader_id)
    ## set redis  globally and also define a separate configuration file for redis
    ##redis = Redis.new()
    ## A sleep is required so that the process of a file is not hinder by a new publish request
    ##
    begin
      basic_files = BasicFile.find(record_ids)
      WorkingLogger.logger.info "[INFO] #{Time.now} --> Starting Publishing of Files - [#{basic_files.collect(&:name).join(",")}] ..." 
      basic_files.collect(&:in_progress!)
      #while($redis.get(basic_file.type_of) && $redis.get(basic_file.type_of) == "locked")
      #  WorkingLogger.logger.info "[DEBUG] #{Time.now} --> A File is Already in Publishing Mode hence sleeping ..."
      #  sleep 50
      #end

      #$redis.set(basic_file.type_of,'locked')
      #WorkingLogger.logger.info "[INFO] #{Time.now} --> Locked Insert #{basic_file.name} Publishing Started ..."
      subscriber_file = SubscriberDownloader.find(downloader_id)
      case typeof 
      when "PartPrice"
        ## Download is no more required I guess
        #download_file_from_s3(subscriber_file.name)
        records = fetch_records(record_ids,klass=PartPriceRecord) + fetch_published_records(typeof,klass=PartPriceRecord)  
        generate_csv(records,PartPriceRecord,subscriber_file.name)
        upload_file_to_s3(subscriber_file.name)
      when "MarketBasket"
        #download_file_from_s3('subscriber_file.name')
        records = fetch_records(record_ids,klass=MarketBasketRecord) + fetch_published_records(typeof,klass=MarketBasketRecord)
        generate_csv(records,MarketBasketRecord,subscriber_file.name)
        upload_file_to_s3(subscriber_file.name)
      end
      #remove_the_lock(basic_file)
      subscriber_file.update_column('status','completed')
      send_an_email(basic_files,typeof)
     rescue => e  
       WorkingLogger.logger.info "[INFO] #{Time.now} Publishing file(#{basic_files.collect(&:name).join(",")}) failed citing reason => #{e}"
       #remove_the_lock(basic_file)
       subscriber_file.destroy
       basic_files.collect(&:marked_as_failed!)
       send_failure_mail(basic_files,e.to_s,typeof)
     end  
  end
  
  def self.fetch_published_records(typeof,klass)
    #[Pending] ## Change the publish status on reupload   -- Done
    record_ids = BasicFile.where('status=? and type_of=?','published',typeof).select(:id).collect(&:id)
    klass.where(:basic_file_id.in => record_ids).without(klass.not_required_attributes)
  end
  
  def self.download_file_from_s3(filename)
    WorkingLogger.logger.info "[INFO] #{Time.now} --> Downloading #{filename} from S3 ..."
    File.open(File.join($SUBSCRIBER_DIRECTORY,filename),'w+') do |file|
      file << $storage.directories.get($BUCKET).files.get("uploads/subscriber/#{filename}").body
    end  
    WorkingLogger.logger.info "[INFO] #{Time.now} --> Downloaded #{filename} from S3 !!!"
  end
  
  def self.fetch_records(record_ids,klass)
    ## Refactor the query below  -- > Done
    klass.where(:basic_file_id.in => record_ids).without(klass.not_required_attributes)
  end
  
  def self.generate_csv(records,klass,file_name)
    WorkingLogger.logger.info "[INFO] #{Time.now} --> CSV parsing initiated for #{file_name} ..."
    CSV.open(File.join($SUBSCRIBER_DIRECTORY,file_name),'w+',:headers => true) do |csv|
      ## seek a file -1 byte from the end to differentiate csv open previously or first time to write the header
      #csv.seek(-1,IO::SEEK_END) rescue csv.add_row(klass.required_attributes)
      csv.add_row(klass.required_attributes.sort)
      records.each do |record|
        csv.add_row(Hash[record.attributes.sort].values)
        record = nil ## To Avoid Memory Bloat 
      end    
    end
    WorkingLogger.logger.info "[INFO] #{Time.now} --> CSV parsing completed for #{file_name} !!!"
  end
  
  def self.upload_file_to_s3(file)
    WorkingLogger.logger.info "[INFO] #{Time.now} -->  Uploading File #{file} to S3 ..."
    SubscriberDownloader.where('name=?',file).first.update_attribute("updated_at",DateTime.now)
    $storage.directories.get($BUCKET).files.create(:key => "uploads/subscriber/#{file}",:body => File.open(File.join($SUBSCRIBER_DIRECTORY,file)))
    
    remove_the_file(file)
    WorkingLogger.logger.info "[INFO] #{Time.now} -->  Uploaded File #{file} to S3 !!!"
  end
  
  def self.remove_the_file(file)
    WorkingLogger.logger.info "[INFO] #{Time.now} -->  Removing #{file} from Local Directory ..."
    FileUtils.rm File.join($SUBSCRIBER_DIRECTORY,file) if File.exists?(File.join($SUBSCRIBER_DIRECTORY,file))
    WorkingLogger.logger.info "[INFO] #{Time.now} -->  Removed #{file} from Local Directory !!!"
  end
  
 # def self.remove_the_lock(basic_file)
 #   WorkingLogger.logger.info "[INFO] #{Time.now} -->  Removing the lock so that other operation and processed for file #{basic_file.name}"
 #   $redis.del(basic_file.type_of)
 # end 
  
  def self.send_an_email(basic_files,typeof)
    WorkingLogger.logger.info "[INFO] Sending a mail for File Publishing completion for #{basic_files.collect(&:name).join(" ")} ..."
    #DeliveryMailer.published_completed(basic_files.collect(&:name),typeof).deliver
    BasicFile.update_all({:published_at => DateTime.now},{:id => basic_files.collect(&:id)})
    basic_files.collect(&:publish!)
    WorkingLogger.logger.info "[INFO] Sending mail for File Publishing completion done for #{basic_files.collect(&:name).join(" ")} !!!"
  end
  
  def self.send_failure_mail(basic_files,error,typeof)
    WorkingLogger.logger.info "[INFO] Sending a Failure Mail for File Publishing for #{basic_files.collect(&:name).join(" ")} ..."
    #DeliveryMailer.published_failure(basic_files.collect(&:name),error,typeof).deliver
    WorkingLogger.logger.info "[INFO] Sending a Failure Mail during the file Publishing done for #{basic_files.collect(&:name).join(" ")} !!!"
  end
 
end
