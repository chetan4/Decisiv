class MarketBasketDumper
  @queue = :market_basket_dumper
  
  def self.perform(record_id,user_id)
    begin
      basicfile = BasicFile.find(record_id)
      basicfile.update_column('import_status','uploading')
      #basicfile.clear_any_validation_errors
      $redis.del("basic_file_#{basicfile.id}")
      download_file_from_s3(basicfile)
      market_basket_attributes = Array.new
      market_basket_records = Array.new  
      deleted_market_basket_records = Array.new
      market_basket_file = true 
      log_records = []
      log_time = nil
      validation_errors = Array.new 
      times = 0
      row_no = 0
      ## CSV.foreach(file,{:headers => true,:header_converters => :symbol}) do |row|
      ##  part_price_attributes =  row.headers if part_price_attributes.empty?
      ##  if PartPriceRecord.new(Hash.new[row.entries]).valid?
      ##    part_price_records  << row.fields
      ##  end
      ## end
      WorkingLogger.logger.info "[INFO] #{Time.now} --> Starting Parsing of Market Basket File Parsing [#{basicfile.name}] ..."
      ## Change the file name
       CSV.foreach(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s,basicfile.asset.filename.to_s)) do |row|
        
        row_no += 1
        row_records = row.collect { |element| element.strip unless element.nil? }
        
        if row.empty? || (row_records.count == 1 && row_records.first.empty?) ||row_records.collect(&:empty?).reduce(:&)
          next
        end
         
        if market_basket_attributes.empty?
          market_basket_attributes = row.collect(&:strip).push(:basic_file_id)
          if market_basket_attributes.include?('us_part_price') || market_basket_attributes.include?('ca_part_price')
            market_basket_file = false
            break
          end
          next
        end
        record_hash = Hash.new
        
        market_basket_attributes.each_with_index do |attrr,index|
          if attrr == :basic_file_id
            record_hash.merge!({:basic_file_id => basicfile.id})
          else
            record_hash.merge!({attrr.to_sym => row[index]})
          end
        end
        record =  MarketBasketRecord.new(record_hash)
         if (row.push(basicfile.id).length == market_basket_attributes.length) && record.valid? 
          #market_basket_records << row
          market_basket_records << record_hash
        else
          WorkingLogger.logger.debug "[DEBUG] #{Time.now} --> A Record was found to be invalid in Market Basket File  [#{basicfile.name}]"
          record.errors.add :base,"Improper Rows fields" if row.length != market_basket_attributes.length 
          #validation_errors  << [row_no,record.errors.messages.to_yaml,basicfile.id]
          validation_errors << {:row_no => row_no,:error_encountered => record.errors.messages.to_yaml}
          #ValidationError.create(:row_id => row_no , :error_encountered => record.errors.messages.to_yaml ,:basic_file_id => basicfile.id)
          times += 1
        end
        record_hash,record,row_records = [nil,nil,nil] ## Make Sure that object create during the current iteration is assigned to nil 
      end
      
      if times.zero?  && market_basket_file
        unless (stale_records = MarketBasketRecord.where(:basic_file_id => basicfile.id)).empty? 
         accumulator = stale_records.clone
         log_time = DateTime.now
         unless accumulator.empty?
           accumulator.collect(&:attributes).collect do |element|
             element.delete("_id")
             element.delete("basic_file_id") 
             log_records << [user_id,"delete","Market Basket","Record",element.to_json,log_time]
             element = nil
           end
         end  
          stale_records.delete_all 
        end
        
        ## CREATE SUSBSCRIBER DOWNLOADER FILE --> DONE
        ##if $redis.get("market_basket_price").nil?
          ## put the params with the modified_file containing current basic_file_id 
        ##  SubscriberDownloader.create(:modified_file => {"id" => [basicfile.id]}.to_json)
        ##  $redis.set("market_basket_price",true)
        ##else
          ## define the method  ==> done
        ##  subscriber_downloader = SubscriberDownloader.latest("market_basket_file").first
          ## define the attribute ==> done
       ##   modified_file = JSON.parse(subscriber_downloader.modified_file)
        ##  modified_file["id"].push(basicfile.id) unless modified_file["id"].include?(basicfile.id) 
        ##  subscriber_downloader.update_column("modified_file",modified_file.to_json)
       ## end
        log_time = DateTime.now
        market_basket_records.uniq.each do |mbr|
          #MarketBasketRecord.collection.insert(market_basket_records)
          log_records << [user_id,"create","Market Basket","Record",mbr.to_json,log_time]
          MarketBasketRecord.create(mbr)
        end
        logging(log_records)
        #MarketBasketRecord.import(market_basket_attributes,market_basket_records)
        basicfile.update_attributes(:import_status => "uploaded",:attempt=> basicfile.attempt+1,:error_field => '' ,:uploaded_at => DateTime.now)
        WorkingLogger.logger.info "[INFO] #{Time.now} --> A Market Basket File [#{basicfile.name}] Parsing was successful "
        market_basket_records,log_records = nil,nil
      elsif market_basket_file
        basicfile.update_attributes(:import_status => 'failed',:attempt => basicfile.attempt+1,:error_field => "#{times} record in the given csv file where incorrect",:uploaded_at => DateTime.now)
        #ValidationError.import([:row_id,:error_encountered,:basic_file_id],validation_errors,:validate => false)
        $redis.set("basic_file_#{basicfile.id}",validation_errors.to_json) 
        validation_errors = nil
        WorkingLogger.logger.debug "[DEBUG] #{Time.now} --> A Market Basket File [#{basicfile.name}] Parsing was failed"
      else 
        basicfile.update_attributes(:import_status => 'failed',:attempt => basicfile.attempt+1,:error_field => "The attributes supplied to the file does not belong look like market basket file attributes",:uploaded_at => DateTime.now)
       WorkingLogger.logger.debug "[DEBUG] #{Time.now} --> A Market Basket File [#{basicfile.name}] Parsing was failed"
      end  
      remove_directory(basicfile)
      send_an_email(basicfile)
     rescue => e
        WorkingLogger.logger.error "[ERROR] #{Time.now} --> The Market Basket File [#{basicfile.name}] could not be parse citing error ==> #{e}"
        basicfile.update_attributes(:import_status => 'failed',:error_field => e.to_s,:uploaded_at => DateTime.now)
        remove_directory(basicfile)
        send_failure_email(basicfile)
      end 
  end
  
   def self.logging(records)
    WorkingLogger.logger.info "INFO #{Time.now} --> Logging the Record for current file "
    Audit.import([:user_id,:operation,:performed_on,:entity,:description,:logged_time],records,:validate => false) unless records.empty?
    WorkingLogger.logger.info "INFO #{Time.now} --> Logged the Record for current file "
  end
 
  def self.download_file_from_s3(basicfile)
    WorkingLogger.logger.info "[INFO]  #{Time.now} --> Downloading Market Basket File from S3 [#{basicfile.name}] ..."
    create_directory(basicfile)
    File.open(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s,basicfile.asset.filename.to_s),'w+') do |file|
      file << $storage.directories.get($BUCKET).files.get(File.join(basicfile.asset.store_dir,basicfile.asset.filename)).body
    end
    WorkingLogger.logger.info "[INFO]  #{Time.now} ---> Downloaded Market Basket File from S3 [#{basicfile.name}] !!!  "
  end
  
  def self.create_directory(basicfile)
    WorkingLogger.logger.info "[INFO]  #{Time.now} ---> Creating directory locally [#{basicfile.name}] ..."
    unless File.directory?(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s))
      FileUtils.mkdir_p File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s)
    end
    WorkingLogger.logger.info "[INFO]  #{Time.now} --> Created Directory locally [#{basicfile.name}] !!!"  
  end
  
  def self.remove_directory(basicfile)
    WorkingLogger.logger.info "[INFO] #{Time.now} --> Removing Directory after processing of Market Basket File [#{basicfile.name}] ..."
    FileUtils.rm_r File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s) if File.exists?(File.join($PUBLISHER_DIRECTORY,basicfile.type_of.to_s,basicfile.id.to_s))
    WorkingLogger.logger.info "[INFO] #{Time.now} --> Removed Directory after processing of Market Basket File [#{basicfile.name}] !!!"
  end
  
  def self.send_an_email(basicfile)
    WorkingLogger.logger.info "[INFO] #{Time.now} Sending email for Market Basket File [#{basicfile.name}] ...  "
    #DeliveryMailer.upload_finished(basicfile).deliver
    WorkingLogger.logger.info "[INFO] #{Time.now} Email Send for Market Basket File [#{basicfile.name}] !!!"
  end
  
  def self.send_failure_email(basicfile)
    WorkingLogger.logger.info "[INFO] #{Time.now} Delivering failure mail of Parsing of Market Basket File [#{basicfile.name}] ..."
    #DeliveryMailer.upload_failure(basicfile).deliver
    WorkingLogger.logger.info "[INFO] #{Time.now} Delivered failure mail of Parsing of Market Basket File [#{basicfile.name}] !!!"
  end
end

