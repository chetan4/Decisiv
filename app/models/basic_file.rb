class BasicFile < ActiveRecord::Base
 extend StateMachine::MacroMethods
  state_machine :status, :initial => :na do
    event :in_progress do
      transition [:na,:failed] => :publishing
    end
    
    event :marked_as_failed do
      transition all => :failed
    end
    
    
    event :publish do
      transition :publishing => :published
    end
  end
  
  
  

  ## naming this to asset as using name file can cause problem with Ruby having a File class
  mount_uploader :asset,FileUploader
  #has_many :part_price_records ,:dependent => :destroy
  #has_many :market_basket_records,:dependent => :destroy
  validates_presence_of :name
  #validates_uniqueness_of :name
  validates_presence_of :asset,:message => "please attach a file to upload"
  #before_save :clear_any_validation_errors ,:if => :not_a_new_record_or_contain_validation_error
  has_many :validation_errors
  #after_save :start_background_processing
  
  
  scope :published_part_price_file ,where("status=? AND type_of=?","published","PartPrice")
  scope :published_market_basket_file ,where('status=? AND type_of=?',"published","MarketBasket")     
  
  def template_to_render
    "new_".concat(type_of.underscore).concat('_file')
  end
  
  def start_background_processing(user_id)
    case self.type_of
    when "MarketBasket"    
      #MarketBasketDumper.perform(self.id,user_id)
      Resque.enqueue(MarketBasketDumper,self.id,user_id)
    when "PartPrice"
      #PartPriceDumper.perform(self.id,user_id)
      Resque.enqueue(PartPriceDumper,self.id,user_id)
    end
  end
  
  def uploaded?
    self.import_status == "uploaded"
  end
  
  def upload_failed?
    self.import_status == "failed"
  end
  
  def self.publish_file(files)   
    #PublishRecordJob.perform(self.id)
    files.group_by(&:type_of).each do |key,value|
      unless value.empty?
        id = create_a_single_file_instance(key).id
        Resque.enqueue(PublishRecordJob,value.collect(&:id),key,id)
        #PublishRecordJob.perform(value.collect(&:id),key,id)
      end   
    end  
  end
  
 
  def clear_any_validation_errors
    ## Using Sql to avoid single delete of record and long awaiting commit from the above realtional query
    ## Wrapping the code is transaction so that the delete record dont occupy in mysql memory
    ## The below would result a separate query for assoicated record
    #self.validation_errors.delete_all
    ValidationError.where('basic_file_id=?',self.id).delete_all
    #self.transaction do
    #  self.connection.execute("delete from validation_errors where basic_file_id=#{self.id}")
    #end
    #self.validation_errors.destroy_all 
  end
  
 
  private_class_method
  def self.create_a_single_file_instance(typecast)
    ## Create the instance
    SubscriberDownloader.create(:name => "#{typecast.underscore}_#{DateTime.now.to_i}.csv",:file => typecast)
  end
  
  
end
