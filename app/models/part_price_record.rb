class PartPriceRecord 
  include Mongoid::Document
    field :supplier_id,type: Integer
    field :part_number,type: String
    field :part_description, type: String
    field :core_indicator,type: String
    field :us_part_price,type: Float
    field :us_core_price,type: Float
    field :us_fleet_price,type: Float
    field :us_distributor_price,type: Float
    field :ca_part_price,type: Float
    field :ca_distributor_price,type: Float
    field :ca_core_price,type: Float
    field :ca_fleet_price,type: Float 
    field :basic_file_id,type: Integer
    index :part_number, unique: true

  validates_presence_of :supplier_id
  validates_presence_of :part_number
  #validates_uniqueness_of :part_number
  validates :part_number ,:format => { :with => /^[a-z0-9A-Z\s*-]+[-a-z0-9\s-]*[a-z0-9\s*-]+$/i ,:message => "Only AlphaNumeric Allowed" }
  validates :supplier_id, :format => { :with => /^[a-z0-9]+[-a-z0-9]*[a-z0-9]+$/i , :message => "Only Alphanumeric Allowed" } 
  #validates :part_description,:presence => true
    
  #validates :part_description,:format => { :with => /^[a-z0-9]+[-a-z0-9]*[a-z0-9]+$/i ,:message => "Only Alphanumberic Allowed"} ,:allow_nil => true
  validates :core_indicator ,:inclusion => { :in => %w(Y N),
    :message => "%{value} is not a valid Coreindicator must be Y | N" 
   } ,:allow_nil => true,:allow_blank => true
  
   
 validates :us_part_price,:us_core_price,:us_fleet_price,:us_distributor_price,:ca_part_price,:ca_core_price,:ca_fleet_price,:ca_distributor_price ,:format => { :with => /^([0-9]+(\.([0-9]{2}|[0-9]{1}))?)$/ ,:message => "should be a numeric value" } ,:allow_nil => true,:allow_blank => true
  
  @@required_attributes =[:supplier_id,:part_number,:part_description,:core_indicator,:us_part_price,:us_core_price,:us_fleet_price,:us_distributor_price,:ca_part_price,:ca_core_price,:ca_fleet_price,:ca_distributor_price]
  @@not_required_attributes = ["_id","basic_file_id"] 

  cattr_reader :required_attributes,:not_required_attributes
 attr_accessible :supplier_id,:part_number,:part_description, :core_indicator,:us_part_price,:us_core_price,:us_fleet_price,:us_distributor_price,:ca_part_price,:ca_distributor_price,:ca_core_price,:ca_fleet_price,:basic_file_id
end
