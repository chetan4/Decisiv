class Audit < ActiveRecord::Base
 Operation = %w(create update destroy publish republish download)
 
 On = %w(PartPrice MarketBasket)
 default_scope order('logged_time desc')
 self.per_page = 20
 
end
