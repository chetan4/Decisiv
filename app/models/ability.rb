class Ability
  include CanCan::Ability

  def initialize(user=current_user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
     if user.publisher?
        can :manage, :all
     else
        cannot [:read,:manage],BasicFile 
        can [:edit,:update],User,:id => user.id
        cannot [:new,:create,:destroy,:index],User
        can [:download_page,:downloadable_part_price_file,:downloadable_market_basket_file,:download_part_price_file,:download_market_basket_file,:download],BasicFile
        cannot [:read,:manage],Customer
        cannot [:read,:manage],Audit
        can :thank_you,:home
     end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
