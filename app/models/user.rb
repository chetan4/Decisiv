class User < ActiveRecord::Base
  
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
 attr_accessible :email,:password,:password_confirmation,:remember_me,:publisher,:first_name,:last_name,:part_price_downloaded_at,:market_basket_downloaded_at
  
  validates :first_name ,:presence => true  
  

  

  
  def deliver_creation_email(password)
    Resque.enqueue(UserCreation,self.id,password)
  end
  
  def name
    first_name+" "+last_name
  end
end
