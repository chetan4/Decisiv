class MarketBasketRecord #< ActiveRecord::Base
  include Mongoid::Document
   
    field :supplier_id, type: String
    field :part_number,type: String
    field :customer_id,type: String
    field :customer_discount,type: Float
    field :price_level,type: String
    field :basic_file_id,type: Integer
   
    
 attr_accessible :supplier_id,:part_number,:customer_id,:customer_discount,:price_level,:basic_file_id
  validates_presence_of :customer_id
  validate :presence_of_customer_id
  validate :supplier_and_part_number
  #validate :part_number_field

  validates :price_level ,:format => { :with => /^([0-9]+(\.([0-9]{2}|[0-9]{1}))?)$/ ,:message => "should look like money" }
  validates :customer_discount , :format => { :with => /^([0-9]+(\.([0-9]{2}|[0-9]{1}))?)$/ ,:message => "should look like money" } ,:allow_nil => true 
  #validate :presence_of_price_level
  @@required_attributes = [:supplier_id,:part_number,:customer_id,:customer_discount,:price_level]
  @@not_required_attributes = ["_id","basic_file_id"]
  cattr_reader :required_attributes,:not_required_attributes
  
 
  private
  
  def presence_of_customer_id
    customers = Customer.where("customer_id = ?",self.customer_id)
    self.errors.add(:customer_id,"Customer Id not Present") if customers.empty?
  end 
  
  #def part_number_field
  #  part_numbers = PartPriceRecord.where(:part_number => self.part_number)
  #  self.errors.add(:part_number,"Part Number not present") if part_numbers.empty?
  #end
  
  def supplier_and_part_number
    records = PartPriceRecord.where(supplier_id: self.supplier_id).and(part_number: self.part_number)
    #supplier_ids = PartPriceRecord.where(:supplier_id => self.supplier_id)
    if records.empty?
      self.errors.add(:base,"supplier_id and part_number combination not present") 
    else
      price_level_validation(records)
    end   
  end
  
  
  def price_level_validation(records)
    unless price_level.nil?
      count = records.any_of({us_part_price: price_level},{us_core_price: price_level},{us_fleet_price: price_level},{us_distributor_price: price_level},{ca_part_price: price_level},{ca_core_price: price_level},{ca_distributor_price: price_level},{ca_fleet_price: price_level}).count()
      self.errors.add(:price_level,"Price Level not present for supplier_id and part_number combination") if count.to_i==0
      count = nil
    end  
  end
end
