module ApplicationHelper

  def content_error(tag,arry)
    wrapper = String.new
    if arry.is_a?(ActiveModel::Errors)
      arry.each do |error,description|
        error = :file if error == :asset 
        wrapper += content_tag(tag,"#{error}:#{description}")
      end
    end
    return wrapper 
  end
  
  def active_class(css_class)
    
    if controller.controller_name == css_class ||  css_class == "#{controller.controller_name}##{controller.action_name}" || css_class == inner_classes
       return "active"
    else
      return ""
    end   
  end
  
  
  def inner_classes
    pages = {
      "basic_files#new_part_price_file" => "basic_files#index",
      "basic_files#new_market_basket_file" => "basic_files#index",
      "basic_files#create" => "basic_files#index",
      "basic_files#edit"   => "basic_files#index",
      "basic_files#show"   => "basic_files#index",
      "basic_files#uploads"  => "basic_files#index",
      "basic_files#downloadable_part_price_file" => "basic_files#download_page",
      "basic_files#downloadable_market_basket_file" => "basic_files#download_page",
      "basic_files#download_part_price_file" => "basic_files#download_page",
      "basic_files#download_market_basket_file" => "basic_files#download_page",
    }   
    pages["#{controller.controller_name}##{controller.action_name}"]
  end

end

css_class = "basic_files#upload"
