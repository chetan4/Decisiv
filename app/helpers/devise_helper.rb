module DeviseHelper

  def devise_error_messages!
     return content_tag :ol,raw(resource.errors.full_messages.inject('') { |resultant,error| resultant += content_tag(:li,error)}),:style => 'padding-top:3px' unless resource.errors.empty?
  end
end
