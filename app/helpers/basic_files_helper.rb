module BasicFilesHelper

  def file_error(file)
   # error = "<span> #{file.error_field} </span> <br/>"
    error = file_content_error(file.validation_errors)
   #error = %q{<ol> <li> Row12 :- Invalid Name</li> </ol> }
   
    return error
  end
  
  def file_content_error(error)
    error_obj = YAML.load(error['error_encountered'])
    "<li> <label class='error_lbl4'>Row_id #{error['row_no']} :- #{error_description(error_obj)} </label> </li>"
  end
  
  def error_description(obj)
    error_field = String.new

    obj.each do |key,value|
      error_field += "#{key} : #{value.join(',')} "
    end
    return error_field
  end
  
  def css_class(css)
    css==controller.action_name ? "active" : ""  
  end
  

end
