## EMAIL VALIDATION For Devise
module EmailAddressValidation
   email_name_regex  = '[A-Z0-9_\.%\+\-]+'
   domain_head_regex = '(?:[A-Z0-9\-]+\.)+'
   domain_tld_regex  = '(?:[A-Z]{2,4}|museum|travel)'
   EMAIL_ADDRESS_EXACT_PATTERN = /\A#{email_name_regex}@#{domain_head_regex}#{domain_tld_regex}\z/i
end

