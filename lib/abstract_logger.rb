
class AbstractLogger
 
  attr_accessor :log_path
  attr_accessor :level
  @severities = {
      :debug   => Logger::DEBUG,
      :info    => Logger::INFO,
      :warn    => Logger::WARN,
      :error   => Logger::ERROR,
      :fatal   => Logger::FATAL,
      :unknown => Logger::UNKNOWN
   }
  class << self
    attr_reader :severities    
  end
  
  
  def initialize(log_path)
    @log_path = log_path
  end 

  def logger
    @logger ||= create_logger
  end  
  
  def logger=(logger)
    @logger = logger
  end
  
  def info(msg)
    add(:info,msg)
  end
  
  def warn(msg)
    add(:warn,msg)
  end
  
  def debug(msg)
    add(:debug,msg)
  end

  def error(msg)
   add(:error,msg)	
  end
   
  def add(level,msg)
    self.logger.add(self.class.severities[level]) { msg }
  end   
  
  def level=(level)
    @level = level
  end
  
  def close
    self.logger.close
  end
  
  private
   def create_logger
     l = Logger.new(log_path)
   end
end


class WorkingLogger
  class << self 
    def logger
      @logger 
    end
    
   def logger=(logger)
     @logger = logger
   end 
  end
end

