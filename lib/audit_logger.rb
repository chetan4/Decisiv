module AuditLogger
  def audit_log(operation,on,description=nil,user=current_user,logged_in_at=DateTime.now)
    Rails.logger.warn "Logging Your Current Action"
    values = []
    if on.kind_of?(String)
      values << [user.id,operation,on,"File",description,logged_in_at]
    else   
       on.collect(&:type_of).each do |iterator|
         values << [user.id,operation,iterator,"File","#{user.name} publish a #{iterator}File",logged_in_at]
       end  
    end
    Audit.import([:user_id,:operation,:performed_on,:entity,:description,:logged_time],values,:validate => false)
  end

end

#audit_log("update",@basic_file.type_of,"#{current_user.name} created a #{@basic_file.type_of}File")
