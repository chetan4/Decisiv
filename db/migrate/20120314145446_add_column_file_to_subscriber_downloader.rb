class AddColumnFileToSubscriberDownloader < ActiveRecord::Migration
  def change
    create_table :subscriber_downloaders do |t|
     t.string :file
     t.timestamps
    end
  end
end
