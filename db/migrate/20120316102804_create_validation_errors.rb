class CreateValidationErrors < ActiveRecord::Migration
  def change
    create_table :validation_errors do |t|
      t.integer :basic_file_id
      t.integer :row_id
      t.string :error_encountered
      t.string :row_id

      t.timestamps
    end
  end
end
