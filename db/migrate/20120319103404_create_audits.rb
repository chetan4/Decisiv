class CreateAudits < ActiveRecord::Migration
  def change
    create_table :audits do |t|
      t.string :operation
      t.text :description
      t.datetime :logged_time
      t.string :performed_on
      t.string :entity
      t.integer :user_id

      t.timestamps
    end
  end
end
