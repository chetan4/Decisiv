class CreateBasicFiles < ActiveRecord::Migration
  def change
    create_table :basic_files do |t|
      t.string :name
      t.integer :attempt ,:default => 0
      t.string :import_status ,:default=> "na"
      t.string :error_field
      t.string :status,:default => "na"
      t.datetime :uploaded_at
      t.datetime :published_at
      t.timestamps
    end
  end
end
