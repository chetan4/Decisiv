class AddColumnMoreFileToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :publisher,:boolean
    add_column :users, :part_price_downloaded_at,:datetime
    add_column :users, :market_basket_downloaded_at,:datetime 
  end
end
