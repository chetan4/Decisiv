class AddColumnToSubscriberDownloader < ActiveRecord::Migration
  def change
    add_column :subscriber_downloaders, :name, :string
    add_column :subscriber_downloaders,:status,:string
  end
end
