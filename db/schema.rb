# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120503123515) do

  create_table "audits", :force => true do |t|
    t.string   "operation"
    t.text     "description"
    t.datetime "logged_time"
    t.string   "performed_on"
    t.integer  "user_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "basic_files", :force => true do |t|
    t.string   "name"
    t.integer  "attempt",       :default => 0
    t.string   "import_status", :default => "na"
    t.string   "error_field"
    t.string   "status",        :default => "na"
    t.datetime "uploaded_at"
    t.datetime "published_at"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "type_of"
    t.string   "asset"
  end

  create_table "customers", :force => true do |t|
    t.string   "name"
    t.string   "customer_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  #create_table "market_basket_records", :force => true do |t|
  #  t.string   "supplier_id"
  #  t.string   "part_number"
  #  t.string   "customer_id"
  #  t.float    "customer_discount"
  #  t.integer  "basic_file_id"
  #  t.datetime "created_at",        :null => false
  #  t.datetime "updated_at",        :null => false
  #  t.string   "price_level"
  #end

  #create_table "part_price_records", :force => true do |t|
  #  t.string   "supplier_id"
  #  t.string   "part_number"
  #  t.text     "part_description"
  #  t.string   "core_indicator"
  #  t.float    "us_part_price"
  #  t.float    "us_core_price"
  #  t.float    "us_fleet_price"
  #  t.float    "us_distributor_price"
  #  t.integer  "basic_file_id"
  #  t.datetime "created_at",           :null => false
  #  t.datetime "updated_at",           :null => false
  #  t.float    "ca_part_price"
  #  t.float    "ca_distributor_price"
  #  t.float    "ca_core_price"
  #  t.float    "ca_fleet_price"
  #end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "subscriber_downloaders", :force => true do |t|
    t.string   "file"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "type_of"
    t.text     "modified_file"
    t.string   "name"
    t.string   "status"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                       :default => "", :null => false
    t.string   "encrypted_password",          :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",               :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.boolean  "publisher"
    t.datetime "part_price_downloaded_at"
    t.datetime "market_basket_downloaded_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "validation_errors", :force => true do |t|
    t.integer  "basic_file_id"
    t.integer  "row_id"
    t.string   "error_encountered"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

end
