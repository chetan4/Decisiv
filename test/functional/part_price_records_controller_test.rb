require 'test_helper'

class PartPriceRecordsControllerTest < ActionController::TestCase
  setup do
    @part_price_record = part_price_records(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:part_price_records)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create part_price_record" do
    assert_difference('PartPriceRecord.count') do
      post :create, part_price_record: @part_price_record.attributes
    end

    assert_redirected_to part_price_record_path(assigns(:part_price_record))
  end

  test "should show part_price_record" do
    get :show, id: @part_price_record
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @part_price_record
    assert_response :success
  end

  test "should update part_price_record" do
    put :update, id: @part_price_record, part_price_record: @part_price_record.attributes
    assert_redirected_to part_price_record_path(assigns(:part_price_record))
  end

  test "should destroy part_price_record" do
    assert_difference('PartPriceRecord.count', -1) do
      delete :destroy, id: @part_price_record
    end

    assert_redirected_to part_price_records_path
  end
end
