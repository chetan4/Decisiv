require 'test_helper'

class MarketBasketRecordsControllerTest < ActionController::TestCase
  setup do
    @market_basket_record = market_basket_records(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:market_basket_records)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create market_basket_record" do
    assert_difference('MarketBasketRecord.count') do
      post :create, market_basket_record: @market_basket_record.attributes
    end

    assert_redirected_to market_basket_record_path(assigns(:market_basket_record))
  end

  test "should show market_basket_record" do
    get :show, id: @market_basket_record
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @market_basket_record
    assert_response :success
  end

  test "should update market_basket_record" do
    put :update, id: @market_basket_record, market_basket_record: @market_basket_record.attributes
    assert_redirected_to market_basket_record_path(assigns(:market_basket_record))
  end

  test "should destroy market_basket_record" do
    assert_difference('MarketBasketRecord.count', -1) do
      delete :destroy, id: @market_basket_record
    end

    assert_redirected_to market_basket_records_path
  end
end
