require 'test_helper'

class BasicFilesControllerTest < ActionController::TestCase
  setup do
    @basic_file = basic_files(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:basic_files)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create basic_file" do
    assert_difference('BasicFile.count') do
      post :create, basic_file: @basic_file.attributes
    end

    assert_redirected_to basic_file_path(assigns(:basic_file))
  end

  test "should show basic_file" do
    get :show, id: @basic_file
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @basic_file
    assert_response :success
  end

  test "should update basic_file" do
    put :update, id: @basic_file, basic_file: @basic_file.attributes
    assert_redirected_to basic_file_path(assigns(:basic_file))
  end

  test "should destroy basic_file" do
    assert_difference('BasicFile.count', -1) do
      delete :destroy, id: @basic_file
    end

    assert_redirected_to basic_files_path
  end
end
