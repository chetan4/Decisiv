Decisiv::Application.routes.draw do

  #root :to => "home#index"
  
  match '/welcome' => "home#index",:as => :home
  
  match '/thank_you' => "home#thank_you" ,:as => :thank_you
  get "home/index"
  
  
  resources :customers

  resources :market_basket_records

  resources :part_price_records

 
  resources :basic_files do 
    collection do
       get 'new_market_basket_file'
       get 'new_part_price_file'
       get 'download'
       post 'publish'  
       get 'publish_completed'
       get 'publish_failed'
       get 'publish_in_progress'
       
    end
    member do
      get 'validation_error' 
      get 'republish'
    
      #get 'download_market_basket_file' ,:as => "download_market_basket_file"
    end
  end

  match '/uploads'  => 'basic_files#uploads' ,:as => :upload ,:via => :get  
  
  match '/progress_upload' => 'basic_files#upload_in_progress',:as => :in_progress_upload ,:via => :get
  match '/failed_upload' => 'basic_files#upload_failed',:as => :failed_upload ,:via => :get
  
  match '/download_page' => "basic_files#download_page",:as => "download_page"
  
  match '/downloadable_part_price_file' => "basic_files#downloadable_part_price_file",:as => "downloadable_part_price_file"
  
  match '/downloadable_market_basket_file' => "basic_files#downloadable_market_basket_file",:as => "downloadable_market_basket_file"  
  
  match "/download" => "basic_files#download" ,:as => :download
  # match '/download_part_price_file' => "basic_files#download_part_price_file",:as => "download_part_price_file"
  
 # match '/download_market_basket_file' => "basic_files#download_market_basket_file",:as => "download_market_basket_file"   
  
  devise_for :users
  
  devise_scope :user do
    get "/" => "devise/sessions#new",:as => :login 
    get "/logout" => "devise/sessions#destroy",:as => :logout

  end
  

  resources :users

 
  
  match '/audits' => 'audits#index' ,:as => :audits
  
  match '/export_to_xls' => 'audits#export_to_xls' ,:as => :export_to_xls,:via => :post
  
  
  mount Resque::Server.new, :at => "/resque"
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
