require File.expand_path('../boot', __FILE__)

require 'rails/all'
ENV['RAILS_ENV'] ||= 'staging'
if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module Decisiv
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/lib)

    require 'audit_logger'
    require 'abstract_logger'
    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    #config.active_record.observers =  :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.mongoid.logger = Logger.new(File.join(Rails.root,'log/mongo.log'))
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]
    config.generators do |g| 
      g.orm :active_record 
    end     
    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    # config.active_record.schema_format = :sql
    require 'csv'
    require "resque/server"
    require 'fileutils'
    # Enforce whitelist mode for mass assignment.
    # This will create an empty whitelist of attributes available for mass-assignment for all models
    # in your app. As such, your models will need to explicitly whitelist or blacklist accessible
    # parameters by using an attr_accessible or attr_protected declaration.
    # config.active_record.whitelist_attributes = true
    #$model = ["part_price_record.rb","market_basket_record.rb"]
    $PROVIDER = 'AWS'
    $ACCESS_KEY_ID =  'AKIAJJXT7ATY4VBO4ZCQ'
    $SECRET_ACCESS_KEY = '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'
    $BUCKET = 'decisv'
    $PUBLISHER_DIRECTORY = '/mnt/decisv/publisher'
    $SUBSCRIBER_DIRECTORY = '/mnt/decisv/subscriber'
    $storage = Fog::Storage.new({:provider => 'AWS', :aws_access_key_id => $ACCESS_KEY_ID, :aws_secret_access_key => $SECRET_ACCESS_KEY})
     if File.exists?(File.join(Rails.root,'config','redis.yml'))
      $redis =  Redis.new()
      ## Need to applying on configuration
      #$redis = Redis.new(YAML::load_file(File.join(Rails.root,'config','redis.yml')))
     else
       $redis =  Redis.new()
     end
    $CONFIG = YAML::load_file(File.join(Rails.root,'config','database.yml'))[Rails.env]
    FileUtils.mkdir_p $PUBLISHER_DIRECTORY unless File.exists?($PUBLISHER_DIRECTORY) && File.directory?($PUBLISHER_DIRECTORY)
    FileUtils.mkdir_p $SUBSCRIBER_DIRECTORY unless File.exists?($SUBSCRIBER_DIRECTORY) && File.directory?($SUBSCRIBER_DIRECTORY)
    # Enable the asset pipeline
    config.assets.enabled = true

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.0'
  end
end
WorkingLogger.logger = AbstractLogger.new(File.join(Rails.root,'log/working.log'))
                     
